# Базовый образ Nginx
FROM nginx

# Копирование содержимого проекта в корневую директорию по умолчанию от Nginx
COPY . /usr/share/nginx/html

# Делаем порт доступным для Nginx
EXPOSE 80
